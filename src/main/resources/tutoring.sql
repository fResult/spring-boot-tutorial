create database tutoring;
USE tutoring;
SHOW TABLES;

CREATE TABLE users (
	id	VARCHAR(10),
    password VARCHAR(16) NOT NULL,
    PRIMARY KEY (id)
);

describe users;

