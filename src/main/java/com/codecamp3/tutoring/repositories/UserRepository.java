package com.codecamp3.tutoring.repositories;

import com.codecamp3.tutoring.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {
  List<User> findAll();
  Optional<User> findById(String id);

  Optional<User> findByIdAndPassword(String id, String password);
}
