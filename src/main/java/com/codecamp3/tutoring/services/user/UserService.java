package com.codecamp3.tutoring.services.user;

import com.codecamp3.tutoring.entities.User;
import com.codecamp3.tutoring.repositories.UserRepository;
import com.codecamp3.tutoring.services.jsonMapperService.JsonMapper;
import com.codecamp3.tutoring.services.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {
  @Autowired
  UserRepository userRepository;
  @Autowired
  SecurityService securityService;
  @Autowired
  JsonMapper jsonMapper;

  public List<User> findAll() {
    List<User> users = userRepository.findAll();
    return users;
  }

  public Optional<User> findById(String id) {
    return userRepository.findById(id);
  }

  public String login(String id, String password) {
    Optional<User> optUser = userRepository.findByIdAndPassword(id, password);
    return securityService.checkUserNameAndPassword(optUser);
  }

  public String login(String idAndPwdJson) {
    User user = jsonMapper.deserialize(idAndPwdJson);
    Optional<User> optUser = userRepository.findByIdAndPassword(user.getId(), user.getPassword());
    return securityService.checkUserNameAndPassword(optUser);
  }

}
