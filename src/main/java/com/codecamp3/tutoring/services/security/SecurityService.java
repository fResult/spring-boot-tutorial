package com.codecamp3.tutoring.services.security;

import com.codecamp3.tutoring.entities.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SecurityService {

  public String checkUserNameAndPassword(Optional<User> optUser) {
    String result;

    //ตรวจสอบว่าค่า User ไม่ null
    if (optUser.isPresent()) {
      result = "Login Successfully!";
    } else {
      result = "Username or Password is invalid.";
    }

    return result;
  }
}
