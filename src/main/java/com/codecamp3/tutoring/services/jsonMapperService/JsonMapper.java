package com.codecamp3.tutoring.services.jsonMapperService;

import com.codecamp3.tutoring.entities.User;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class JsonMapper {
  // Deserialize คือการทำให้ json string แปลงเป็น Object
  public User deserialize(String json) {
    System.out.println(json);
    User user = null;
    ObjectMapper mapper = new ObjectMapper();
    try {
      // หาวิธีทำให้ read json ออกมาเป็น Object แบบ Dynamic type ยังไม่ได้
      // จึงให้ Parse เป็น Object ของ User ไปก่อน
      user = mapper.readValue(json, User.class);
    } catch (JsonParseException e) {
      e.printStackTrace();
    } catch (JsonMappingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return user;
  }

  // Serialize คือการทำให้ Object แปลงเป็น Json string
  public String serialize() {
    ObjectMapper mapper = new ObjectMapper();
    return "";
  }
}
