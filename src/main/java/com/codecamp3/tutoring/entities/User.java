package com.codecamp3.tutoring.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {

  @Id
  @Column(name = "id")
  private String id;
  @Column(name = "password")
  private String password;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public String toString() {
    return "User{" +
      "id='" + id + '\'' +
      ", password='" + password + '\'' +
      '}';
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
