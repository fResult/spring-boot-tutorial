package com.codecamp3.tutoring.controllers;

import com.codecamp3.tutoring.entities.User;
import com.codecamp3.tutoring.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

  @Autowired
  UserService userService;

  @GetMapping("/users")
  public List<User> findAllUser() {
    List<User> users = userService.findAll();

    return users;
  }

  @GetMapping("/user/{id}/{password}")
  public Optional<User> findUserById(@PathVariable String id) {
    Optional<User> user = userService.findById(id);
    System.out.println(user);
    return user;
  }

  // ท่าง่าย: Login ผ่าน URI (เป็นวิธีที่ไม่เหมาะสม เพราะ Hacker สามารถ Hack ข้อมูลไปได้ง่าย เพียงแค่ดักอ่านค่าผ่าน URI)
  @GetMapping("/user/login")
  public String loginPassByUri(@RequestParam String id, @RequestParam String password) {
    return userService.login(id, password);
  }

  // ท่ายากขึ้น: Login โดยส่งค่า id&password ผ่าน Body เป็นข้อความ JSON มาให้ backend
  // แต่เอาเข้าจริงก็ยะังขาดเรื่องการทำ Hashing เพื่อเข้ารหัส password แบบไม่สามารถถอดกลับมาเป็นค่าเดิมได้
  // การทำเช่นนั้น จะทำให้ไม่สามารถ Hack ข้อมูลจากการเข้าไปอ่าน Field password ในตาราง users ได้
  @PostMapping("/user/login")
  public String loginPassByBody(@RequestBody String idAndPassword) {
    return userService.login(idAndPassword);
  }
}